import { PasswordValidators } from './validators/password.validators';
import { Countries } from './interfaces/country';
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import * as COUNTRIES from './data/countries.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  todayDate = new Date().toLocaleDateString('en-ca');
  data: Countries = COUNTRIES;
  selectedCountry!: number;

  form = this.fb.group({
    information: this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.pattern(/^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$/),
        ],
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.minLength(4),
          Validators.pattern(/^([A-Z][a-z]+([ ]?[a-z]?['-]?[A-Z][a-z]+)*)$/),
        ],
      ],
      birthDate: ['', Validators.required],
    }),
    address: this.fb.group({
      country: ['', Validators.required],
      state: ['', Validators.required],
    }),
    contact: this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      telephone: ['', [Validators.required, Validators.minLength(8)]],
    }),
    password: this.fb.group(
      {
        password: [
          '',
          [
            Validators.required,
            Validators.pattern(/^(?=.*d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/),
          ],
        ],
        confirmPassword: ['', [Validators.required]],
      },
      { validators: PasswordValidators.passwordDoesntMatch }
    ),
    gender: ['', Validators.required],
    site: [
      '',
      Validators.pattern(
        /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/
      ),
    ],
    aboutMe: ['', Validators.pattern(/[A-Za-z 0-9\.,;:!?()&\n\-]+/)],
    agree: ['', Validators.required],
  });

  constructor(private fb: FormBuilder) {}

  get information(): AbstractControl {
    return this.form.get('information')!;
  }

  get address(): AbstractControl {
    return this.form.get('address')!;
  }

  get contact(): AbstractControl {
    return this.form.get('contact')!;
  }

  get passwordGroup(): AbstractControl {
    return this.form.get('password')!;
  }

  setSelectedCountry(selectedCountry: string): void {
    this.selectedCountry = parseInt(selectedCountry);
  }

  validateDate(): void {
    const birthDate = this.information.get('birthDate')!;

    if (birthDate.value > this.todayDate) {
      birthDate.setValue(this.todayDate);
    }
  }

  validateNumbers(e: KeyboardEvent): void {
    if (!Number.isInteger(parseInt(e.key))) {
      e.preventDefault();
    }
  }

  submit(): void {
    console.log(this.form.value);
  }
}
