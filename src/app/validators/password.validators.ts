import { ValidationErrors, AbstractControl } from '@angular/forms';

export class PasswordValidators {
  static passwordDoesntMatch(control: AbstractControl): ValidationErrors | null {
    if (control.get('password')!.value != control.get('confirmPassword')!.value) {
      return {
        passwordsDoesntMatch: true,
      };
    }

    return null;
  }
}
