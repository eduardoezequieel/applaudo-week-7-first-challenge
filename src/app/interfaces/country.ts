export interface Countries {
  countries: Country[];
}

export interface Country {
  country: string;
  states: string[];
}